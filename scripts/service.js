angular.module('mainService', [])

.service('svc', ['$http', function($http) {

	var baseUrl = "service";

	return {

		getEvents: function(date) {

			return $http({
				method: 'GET',
				url: baseUrl + '/get-events.php',
			});

		},

		getEventsByDate: function(date) {

			return $http({
				method: 'GET',
				url: baseUrl + '/get-events.php?date=' + date
			});

		},

		getEventsById: function(id) {

			return $http({
				method: 'GET',
				url: baseUrl + '/get-events.php?id=' + id
			});

		},

		getEventsByType: function(type) {

			return $http({
				method: 'GET',
				url: baseUrl + '/get-events.php?type=' + type
			});

		}

	};
}]);