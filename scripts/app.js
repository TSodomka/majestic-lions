angular.module("main", ['dateTimePicker', 'mainService'])
	.controller('MainCtrl', ['$scope', 'svc', function($scope, svc) {
		$scope.data = {
			enabledDates: [],
			loading: true,
			selectedEvent: null
		};

		svc.getEvents().then(function(resp) {
			if (resp.data.status === 'ok') {
				$scope.data.events = resp.data.events;
				if ($scope.data.events && $scope.data.events.length) {
					for (var i = 0; i < $scope.data.events.length; i++) {
						var mom = moment($scope.data.events[i].date);
						$scope.data.enabledDates.push(mom);
						$scope.data.events[i]._moment = mom;
					}
				}
			}
			$scope.data.loading = false;
		});

		$scope.selectEvent = function(mom) {
			for (var i = 0; i < $scope.data.events.length; i++) {

				if ($scope.data.events[i]._moment.isSame(mom, 'day')) {
					$scope.data.selectedEvent = $scope.data.events[i];
				}
			}

			if ($scope.data.date && $scope.data.date !== mom) {
				$scope.data.date = mom;
			}
		};

		$scope.$watch('data.date', function(newVal) {
			if (newVal) {
				$scope.selectEvent(newVal);
			}
		});

	}]);