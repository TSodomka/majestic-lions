<?php

define("HOST", "innodb.endora.cz");     // host
define("USER", "web");    // database username.
define("PASSWORD", "majlions");    // database password
define("DATABASE", "calendar");    // database name

$request = json_decode(file_get_contents("php://input"), true);

if (isset($request['id'])) {
	$param = $request['id'];
	$prep_stmt = "SELECT id, date, type, name, description, visible FROM events WHERE id = ?";
} else if (isset($request['date'])) {
	$param = $request['date'];
	$prep_stmt = "SELECT id, date, type, name, description, visible FROM events WHERE date = ?";
} else if (isset($request['type'])) {
	$param = $request['type'];
	$prep_stmt = "SELECT id, date, type, name, description, visible FROM events WHERE type = ?";
} else {
	$param = 1;
	$prep_stmt = "SELECT id, date, type, name, description, visible FROM events WHERE visible = ?";
}


$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
$stmt = $mysqli->prepare($prep_stmt);

if ($stmt) {
    $stmt->bind_param('s', $param);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows == 0) {
       
       	$stmt->close();
        $obj = (object) array (
             'id' => '0',
             'status' => 'fail',
             'message' => 'Event nenalezen.'
         );
        echo json_encode($obj);

    } else {
    	$stmt->bind_result($id, $date, $type, $name, $description, $visible);

    	$eventsObj = (object) array (
    		'events' => array(),
    		'status' => 'ok'
    	);

    	while ($stmt->fetch()) {

    	    $obj = (object) array (      	
    	         'id' => $id,
    	         'date' => $date,
    	         'type' => $type,
    	         'name' => $name,
    	         'description' => $description,
    	         'visible' => $visible             
    	     );    

    	    array_push($eventsObj->events, $obj);

    	}   

    	$stmt->close();

    	echo json_encode($eventsObj);
    }
 
} else {
    $stmt->close();
    $obj = (object) array (
         'id' => '0',
         'status' => 'fail',
         'message' => 'Chyba v přístupu do DB, kontaktujte správce webu.'
     );
    echo json_encode($obj);
}

?>